package zeefeld.java.FibonacciSeries;

import java.math.BigInteger;
import java.util.concurrent.RecursiveTask;


@SuppressWarnings("serial")
public class ConcurrentFibonacci extends RecursiveTask<BigInteger>
{
	public Fibonacci calculation;
	public BigInteger calculatedResult;
	
	public ConcurrentFibonacci(Fibonacci calculation)
	{
		this.calculation = calculation;
	}	
	
		@Override
		public BigInteger compute() 
		{
			if (calculation.n == 0)
			{
				return BigInteger.ZERO;
			}
			if (calculation.n == 1)
			{
				return BigInteger.ONE;
			}
			       
			ConcurrentFibonacci f1 = new ConcurrentFibonacci
											(new Fibonacci(calculation.n - 1));
			
			ConcurrentFibonacci f2 = new ConcurrentFibonacci
											(new Fibonacci(calculation.n - 2));
			
			f1.fork();
			calculatedResult = f2.compute().add(f1.join());
			return calculatedResult;
		}
	
}

package zeefeld.java.FibonacciSeries;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;

public class Fibonacci 
{
	public int n;
	private static ArrayList<BigInteger> cache = new ArrayList<BigInteger>();
	static 
	{
	      cache.add(BigInteger.ZERO);
	      cache.add(BigInteger.ONE);
	}
	
	public Fibonacci(){}
	public Fibonacci(int n)
	{
		this.n = n;
	}
	
	/**
	 * Calculate Fibonacci numbers using a iterative approach.
	 * 
	 * @param n nth Fibonacci number to calculate
	 * @return nth Fibonacci number
	 */
	public static BigInteger fibIterate(int n)
	{
		BigInteger a = BigInteger.ZERO;
		BigInteger b = BigInteger.ONE;
		
		for (int i = 0; i < n; i++)
		{
			BigInteger c = a.add(b);
			a = b;
			b = c;
		}
		return a;
	} 
	
	/**
	 * Recursive Fibonacci solution.
	 * 
	 * @param n nth Fibonacci number
	 * @return nth Fibonacci number
	 */
	public static BigInteger fibRecurse(int n)
	{
		
		if (n == 0)
		{
			return BigInteger.ZERO;
		}
		if (n == 1)
		{
			return BigInteger.ONE;
		}
		
		return fibRecurse(n - 1).add(fibRecurse(n - 2));
	}
	
	public static BigInteger fibMemoization(int n) 
	{
		
		if (n >= cache.size())
        {
            cache.add(fibMemoization(n-1).add(fibMemoization(n-2)));
        }
		return cache.get(n);
	}
	
	public static BigInteger fibConcurrent(int n)
	{
		Fibonacci f = new Fibonacci(n);
		ConcurrentFibonacci calculation = new ConcurrentFibonacci(f);
		int processors = Runtime.getRuntime().availableProcessors();
		ForkJoinPool pool = new ForkJoinPool(processors);
		pool.invoke(calculation);
		
		return calculation.calculatedResult;
	}
}

package zeefeld.java.util.Memory;

public class Memory 
{
	private static final long MEGABYTE = 1024L * 1024L;
	
	public static long getMegabytes()
	{
		Runtime runtime = Runtime.getRuntime();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		
		//run garbage collector
		runtime.gc();
		return memory / MEGABYTE;
	}
	
	public static long getBytes()
	{
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		return runtime.totalMemory() - runtime.freeMemory();
	}
	
	public static void runGarbageCollector()
	{
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
	}
	
}

package zeefeld.java.util.StopWatch;

public class StopWatch 
{
	private long startTime;
	private long endTime;
	private long runTime;
	private boolean isRunning;
	
	public StopWatch()
	{
		reset();
	}
	
	public void reset()
	{
		runTime = 0;
		isRunning = false;
	}
	
	public void start()
	{
		if (isRunning)
		{
			return;
		}
		isRunning = true;
		startTime = System.currentTimeMillis();
	}
	
	public void stop()
	{
		if (!isRunning)
		{
			return;
		}
		isRunning = false;
		endTime = System.currentTimeMillis();
		runTime = endTime - startTime;
	}
	
	public long getRunTime()
	{
		return runTime;
	}
}


import java.math.BigInteger;

import zeefeld.java.FibonacciSeries.*;
import zeefeld.java.util.Memory.Memory;
import zeefeld.java.util.StopWatch.*;

public class Tester 
{
	
	public static void main(String[] args)
	{
		System.out.println("Please wait while the compiler warms up...");
		
		//warming up java compiler
		for (int i = 10; i < 30; i++)
		{
			Fibonacci.fibIterate(i);
			Fibonacci.fibRecurse(i);
			Fibonacci.fibMemoization(i);
			Fibonacci.fibConcurrent(i);
		}
		
		System.out.println("Compiler is ready...Computing...");
		System.out.println();
		
		StopWatch stopWatch = new StopWatch();
		
		//run garbage collector
		Memory.runGarbageCollector();
		
		//set nth Fibonacci number
		int n = 45;
		
		//begin running calculations
		stopWatch.start();
		BigInteger f1 = Fibonacci.fibIterate(n);
		stopWatch.stop();
		long f1Time = stopWatch.getRunTime();
		long f1Space = Memory.getBytes(); 
		
		//cleaning up memory space
		Memory.runGarbageCollector();
		
		stopWatch.start();
		BigInteger f2 = Fibonacci.fibRecurse(n);
		stopWatch.stop();
		long f2Time = stopWatch.getRunTime();
		long f2Space = Memory.getBytes();
		
		//cleaning up memory space
		Memory.runGarbageCollector();
		
		stopWatch.start();
		BigInteger f3 = Fibonacci.fibMemoization(n);
		stopWatch.stop();
		long f3Time = stopWatch.getRunTime();
		long f3Space = Memory.getBytes();
		
		//cleaning up memory space
		Memory.runGarbageCollector();
		
		stopWatch.start();
		BigInteger f4 = Fibonacci.fibConcurrent(n);
		stopWatch.stop();
		long f4Time = stopWatch.getRunTime();
		long f4Space = Memory.getBytes();
		
		//cleaning up memory space
		Memory.runGarbageCollector();
		
		//printing out results at end to save on the high cost
		//of standard output
		
		System.out.println("Iterative --> " + f1);
		System.out.println("Total runtime: " + f1Time + " ms");
		System.out.println("Total memory: " + f1Space + " bytes");
		System.out.println();
		System.out.println("Recursive --> " + f2);
		System.out.println("Total runtime: " + f2Time + " ms");
		System.out.println("Total memory: " + f2Space + " bytes");
		System.out.println();
		System.out.println("Memoized Recursive --> " + f3);
		System.out.println("Total runtime: " + f3Time + " ms");
		System.out.println("Total memory: " + f3Space + " bytes");
		System.out.println();
		System.out.println("Concurrent Recursive --> " + f4);
		System.out.println("Total runtime: " + f4Time + " ms");
		System.out.println("Total memory: " + f4Space + " bytes");
	}
}
